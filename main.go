package main

import (
	"flag"
	"bitbucket.org/VaidotasSm/pagination-simple/pagination"
	"fmt"
)

func main() {
	totalPages := flag.Uint("total_pages", 0, "Total pages e.g. 10")
	currentPage := flag.Uint("current_page", 0, "Current page e.g. 5")
	boundaries := flag.Uint("boundaries", 0, "How many pages we want to link in the beginning and in the end")
	around := flag.Uint("around", 0, "How many pages we want to link before and after the actual page")
	flag.Parse()

	pages, err := pagination.GetPages(pagination.Opts{
		TotalPages: *totalPages,
		CurrentPage: *currentPage,
		EndNeighbours: *boundaries,
		PageNeighbours: *around,
	})
	if err != nil {
		fmt.Println("Error while processing request", err.Error())
		return
	}

	fmt.Println(pages.ToString())
}
