# Pagination
Simple pagination page list display implementation.

## Usage
As a commonad line:
```
go run main.go -current_page=4 -total_pages=5 -boundaries=1 -around=0

go run main.go -current_page=4 -total_pages=10 -boundaries=2 -around=2
```

As a library:
```
// Import
import "bitbucket.org/VaidotasSm/pagination-simple/pagination"

// Calculate pages
pages, err := pagination.GetPages(pagination.Opts{
  TotalPages: 5,
  CurrentPage: 2,
  EndNeighbours: 1,
  PageNeighbours: 1,
})

// Operate with pages slice
for _, page := range pages.Pages {
  fmt.Println(page)
}

// Get string representation
pages.ToString()
```

## Technical

### Prerequisites
* Go `1.8+`

### Dependencies
* `github.com/stretchr/testify` - assertions library for testing.

### Run tests
```
go test ./...
```
