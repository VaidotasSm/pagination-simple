package pagination

import (
	"sort"
)

type calculator struct {
	opts *Opts
}

func (c *calculator) calculate() []*uint {
	var start []uint
	var end []uint
	if c.opts.EndNeighbours > 0 {
		start, end = c.calculateStartEnd()
	}

	pages := start
	if c.opts.CurrentPage > 0 {
		current := c.calculateCurrentPage()
		pages = append(pages, current...)
	}
	pages = append(pages, end...)
	return c.polishPages(pages)
}

func (c *calculator) calculateStartEnd() ([]uint, []uint) {
	if c.opts.EndNeighbours == 0 {
		return []uint{}, []uint{}
	}

	start := c.genRange(1, c.opts.EndNeighbours)
	end := c.genRange(c.opts.TotalPages+1-c.opts.EndNeighbours, c.opts.TotalPages)
	return start, end
}

func (c *calculator) calculateCurrentPage() []uint {
	if c.opts.PageNeighbours == 0 {
		return []uint{c.opts.CurrentPage}
	}
	if c.opts.PageNeighbours > c.opts.CurrentPage {
		return c.genRange(
			1,
			c.opts.CurrentPage+c.opts.PageNeighbours,
		)
	}

	return c.genRange(
		c.opts.CurrentPage-c.opts.PageNeighbours,
		c.opts.CurrentPage+c.opts.PageNeighbours,
	)
}

func (c *calculator) polishPages(pages []uint) []*uint {
	sort.Slice(pages, func(i1, i2 int) bool {
		return pages[i1] <= pages[i2]
	})

	var results []*uint
	var lastAdded uint
	for i := 0; i < len(pages); i++ {
		page := pages[i]
		if lastAdded != 0 && lastAdded == page {
			continue
		}
		if lastAdded != 0 && page-lastAdded > 1 {
			results = append(results, nil)
		}
		results = append(results, &page)
		lastAdded = page

	}
	return results
}

func (c *calculator) genRange(from, to uint) []uint {
	var result []uint
	for i := from; i <= to && i <= c.opts.TotalPages; i++ {
		if i >= 1 {
			result = append(result, i)
		}
	}
	return result
}
