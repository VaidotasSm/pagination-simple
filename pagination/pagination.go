package pagination

import (
	"fmt"
	"strings"
	"errors"
)

// GetPages calculates pages to display
func GetPages(opts Opts) (*Pages, error) {
	if opts.TotalPages == 0 {
		return nil, errors.New("TotalPages must be greater than 0")
	}
	if opts.CurrentPage > opts.TotalPages {
		return nil, errors.New("CurrentPage must be lower than TotalPages")
	}

	calc := &calculator{
		opts: &opts,
	}

	return &Pages{
		Pages: calc.calculate(),
	}, nil
}

// Opts is used for page calculation options
type Opts struct {
	TotalPages     uint
	CurrentPage    uint
	PageNeighbours uint
	EndNeighbours  uint
}

// Pages is page calculation result
type Pages struct {
	Pages []*uint
}

// ToString is used to generate string representation of pages info
func (p *Pages) ToString() string {
	var strValues []string
	for _, page := range p.Pages {
		if page == nil {
			strValues = append(strValues, "...")
		} else {
			strValues = append(strValues, fmt.Sprint(*page))
		}
	}

	return strings.Join(strValues, " ")
}
