package pagination

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPagination(t *testing.T) {

	t.Run("should handle invalid input", func(t *testing.T) {
		_, err := GetPages(Opts{})
		assert.Error(t, err)

		_, err = GetPages(Opts{
			TotalPages: 5,
			CurrentPage: 10,
		})
		assert.Error(t, err)
	})

	t.Run("should handle start/end neighbours", func(t *testing.T) {
		assertPages(t, &Opts{
			TotalPages: 1,
			EndNeighbours: 10,
		}, "1", "")

		assertPages(t,
			&Opts{
				TotalPages:     5,
				CurrentPage:    0,
				EndNeighbours:  0,
				PageNeighbours: 0,
			},
			"",
			"zero neighbours",
		)

		assertPages(t,
			&Opts{
				TotalPages:     5,
				CurrentPage:    0,
				EndNeighbours:  1,
				PageNeighbours: 1,
			},
			"1 ... 5",
			"one neighbour",
		)

		assertPages(t,
			&Opts{
				TotalPages:     10,
				CurrentPage:    0,
				EndNeighbours:  3,
				PageNeighbours: 3,
			},
			"1 2 3 ... 8 9 10",
			"3 neighbours",
		)
	})

	t.Run("should handle currentPage neighbours", func(t *testing.T) {
		assertPages(t, &Opts{
			TotalPages: 1,
			CurrentPage: 1,
			PageNeighbours: 10,
		}, "1", "only one page")

		assertPages(t,
			&Opts{
				TotalPages:  5,
				CurrentPage: 3,
			},
			"3",
			"zero neighbours",
		)

		assertPages(t,
			&Opts{
				TotalPages:     5,
				CurrentPage:    3,
				PageNeighbours: 1,
			},
			"2 3 4",
			"one neighbour",
		)

		assertPages(t,
			&Opts{
				TotalPages:     10,
				CurrentPage:    5,
				PageNeighbours: 3,
			},
			"2 3 4 5 6 7 8",
			"three neighbours",
		)
	})

	t.Run("should remove duplicates when overlapping", func(t *testing.T) {
		assertPages(t,
			&Opts{
				TotalPages:     7,
				CurrentPage:    5,
				PageNeighbours: 1,
				EndNeighbours:  2,
			},
			"1 2 ... 4 5 6 7",
			"end overlaps with currentPage",
		)

		assertPages(t,
			&Opts{
				TotalPages:     5,
				CurrentPage:    3,
				PageNeighbours: 3,
				EndNeighbours:  3,
			},
			"1 2 3 4 5",
			"everything overlaps",
		)

	})

	t.Run("should handle too many neighbours", func(t *testing.T) {
		assertPages(t,
			&Opts{
				TotalPages:     3,
				CurrentPage:    1,
				PageNeighbours: 3,
				EndNeighbours:  3,
			},
			"1 2 3",
			"too many neighbours",
		)

		assertPages(t,
			&Opts{
				TotalPages:     5,
				CurrentPage:    3,
				PageNeighbours: 3,
				EndNeighbours:  4,
			},
			"1 2 3 4 5",
			"too many neighbours",
		)

	})
}

func assertPages(t *testing.T, opts *Opts, expected, message string) {
	pages, err := GetPages(*opts)
	assert.NoError(t, err)
	assert.Equal(t, expected, pages.ToString(), message)
}
